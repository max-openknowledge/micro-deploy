#!/bin/sh

VERSION=$(xpath pom.xml /project/version 2>/dev/null | perl -pe 's/^.+?\>//; s/\<.+?$//;')
IMAGE=baroprime/micro-deploy:${VERSION}

echo ${VERSION}
echo ${IMAGE}

mvn clean package

docker build -t ${IMAGE} .
docker push ${IMAGE}

kubectl rolling-update kubectl rolling-update frontend --image=image:v2 --image=${IMAGE}
