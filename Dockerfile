FROM java:openjdk-8-jdk-alpine
COPY "target/micro-deploy.jar" .
EXPOSE 8080
ENTRYPOINT [ "java", "-jar", "micro-deploy.jar"]
