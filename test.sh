#!/bin/sh

sh src/test/resources/prepareTest.sh || echo 'Could not prepare tests'

mvn clean verify

sh src/test/resources/tearDownTest.sh || echo 'Could not teardown tests'

