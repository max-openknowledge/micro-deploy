#!/bin/sh

echo "prepare tests"

# create test namespaces
kubectl create ns test  > /dev/null 2>&1

kubectl create ns test-delete  > /dev/null 2>&1

# create test deployment
cat <<EOF | kubectl apply -n test -f  > /dev/null 2>&1 -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
EOF

# create test-delete deployment
cat <<EOF | kubectl apply -n test-delete -f  > /dev/null 2>&1 -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
EOF

# create test pod
cat <<EOF | kubectl apply -n test -f  > /dev/null 2>&1 -
apiVersion: v1
kind: Pod
metadata:
  name: nginx-pod
spec:
  containers:
    - name: web
      image: nginx:1.7.9
      ports:
        - name: web
          containerPort: 80
          protocol: TCP
EOF

cat <<EOF | kubectl apply -n test-delete -f  > /dev/null 2>&1 -
apiVersion: v1
kind: Pod
metadata:
  name: nginx-pod
spec:
  containers:
    - name: web
      image: nginx:1.7.9
      ports:
        - name: web
          containerPort: 80
          protocol: TCP
EOF

# create test replicaset
cat <<EOF | kubectl apply -n test -f  > /dev/null 2>&1 -
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-replicaset
  labels:
    app: nginx
spec:
  # modify replicas according to your case
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
EOF

cat <<EOF | kubectl apply -n test-delete -f  > /dev/null 2>&1 -
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-replicaset
  labels:
    app: nginx
spec:
  # modify replicas according to your case
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
EOF

# create test secret
kubectl create secret generic nginx-secret --from-literal=username=nginx --from-literal=password=nginx -n test  > /dev/null 2>&1

kubectl create secret generic nginx-secret --from-literal=username=nginx --from-literal=password=nginx -n test-delete  > /dev/null 2>&1

echo 'Done'
