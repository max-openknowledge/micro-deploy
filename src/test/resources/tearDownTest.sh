#!/bin/sh

echo "tear down test objects"

kubectl delete deploy --all -n test  > /dev/null 2>&1

kubectl delete deploy --all -n test-delete  > /dev/null 2>&1


kubectl delete pod --all -n test  > /dev/null 2>&1

kubectl delete pod --all -n test-delete  > /dev/null 2>&1


kubectl delete ns test  > /dev/null 2>&1

kubectl delete ns test-delete  > /dev/null 2>&1

echo "Done"
