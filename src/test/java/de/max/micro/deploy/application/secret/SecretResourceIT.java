package de.max.micro.deploy.application.secret;

import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.ws.rs.core.MediaType;
import java.util.HashMap;

import static de.max.micro.deploy.infrastructure.constants.Constants.DEFAULT_SECRET;
import static de.max.micro.deploy.infrastructure.constants.Constants.NAMESPACE;
import static de.max.micro.deploy.infrastructure.constants.Constants.NAMESPACE_DELETE;
import static de.max.micro.deploy.infrastructure.constants.Constants.PATH_SECRET;
import static de.max.micro.deploy.infrastructure.constants.Constants.URI;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SecretResourceIT {

  @BeforeAll
  public void setUp() {
    RestAssured.baseURI = URI + PATH_SECRET;
  }

  @Test
  public void testGetSecret() {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getDefaultURI(NAMESPACE))
        .then()
        .statusCode(200);
  }

  @Test
  public void testGetSecretWrongNamespace() {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getDefaultURI("a"))
        .then()
        .statusCode(404);
  }

  @Test
  public void testGetSecretWrongSecret() {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getURI("a", NAMESPACE))
        .then()
        .statusCode(400);
  }

  @Test
  public void testGetAllSecrets() {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getListURI(NAMESPACE))
        .then()
        .contentType(MediaType.APPLICATION_JSON)
        .body("size()", Matchers.is(2))
        .statusCode(200);
  }

  @Test
  public void testGetAllSecretsWrongNamespace() {
    RestAssured
        .given()
        .when()
        .get(getListURI("a"))
        .then()
        .statusCode(400);
  }

  @Test
  public void testDeleteSecret() {
    RestAssured
        .given()
        .when()
        .delete(getDefaultURI(NAMESPACE_DELETE))
        .then()
        .statusCode(204);
  }

  @Test
  public void testDeleteSecretFailWrongNamespace() {
    RestAssured
        .given()
        .when()
        .delete(getDefaultURI("a"))
        .then()
        .statusCode(404);
  }

  @Test
  public void testDeleteSecretFailWrongPod() {
    RestAssured
        .given()
        .when()
        .delete(getURI("a", NAMESPACE))
        .then()
        .statusCode(400);
  }

  @Test
  public void testCreateSecret() {
    HashMap<String, String> data = new HashMap<>();
    data.put("username", "test");
    data.put("password", "test");

    SecretDTO secretDTO = new SecretDTO();
    secretDTO.setName("test-secret");
    secretDTO.setData(data);
    secretDTO.setLabels(new HashMap<>());

    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .body(secretDTO)
        .when()
        .post(postURI(NAMESPACE_DELETE))
        .then()
        .statusCode(201);
  }

  @Test
  public void testCreateSecretWrongNamespace() {
    RestAssured
        .given()
        .when()
        .post(postURI("a"))
        .then()
        .statusCode(400);
  }

  private String getURI(final String secret, final String namespace) {
    return "/" + secret + "?namespace=" + namespace;
  }

  private String getDefaultURI(final String namespace) {
    return DEFAULT_SECRET + "?namespace=" + namespace;
  }

  private String getListURI(final String namespace) {
    return "?namespace=" + namespace;
  }

  private String postURI(final String namespace) {
    return "?namespace=" + namespace;
  }
}
