package de.max.micro.deploy.application.deployment;

import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;

import static de.max.micro.deploy.infrastructure.constants.Constants.DEFAULT_DEPLOYMENT;
import static de.max.micro.deploy.infrastructure.constants.Constants.NAMESPACE;
import static de.max.micro.deploy.infrastructure.constants.Constants.NAMESPACE_DELETE;
import static de.max.micro.deploy.infrastructure.constants.Constants.PATH_DEPLOYMENT;
import static de.max.micro.deploy.infrastructure.constants.Constants.URI;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DeploymentResourceIT {

  @BeforeAll
  public void setUp() {
    RestAssured.baseURI = URI + PATH_DEPLOYMENT;
  }

  @Test
  public void testGetAll() {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get("?namespace=" + NAMESPACE)
        .then()
        .statusCode(200);
  }

  @Test
  public void testGetAllSizeShouldBeOne() {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get("?namespace=" + NAMESPACE)
        .then()
        .contentType(MediaType.APPLICATION_JSON)
        .body("size()", Matchers.is(1))
        .statusCode(200);
  }

  @Test
  public void testGetAllShouldFailForWrongNamespace() {
    RestAssured
        .given()
        .when()
        .get("?namespace=default2")
        .then()
        .statusCode(400);
  }

  @Test
  public void testGetOne() {
    RestAssured
        .given()
        .when()
        .get(getDefaultURI(NAMESPACE))
        .then()
        .contentType(MediaType.APPLICATION_JSON)
        .body("metadata.name", Matchers.is("nginx-deployment"))
        .statusCode(200);
  }

  @Test
  public void testGetOneWrongNamespaceShouldFail() {
    RestAssured
        .given()
        .when()
        .get(getDefaultURI("kube-system2"))
        .then()
        .statusCode(404);
  }

  @Test
  public void testDeleteOne() {
    RestAssured
        .given()
        .when()
        .delete(getDefaultURI(NAMESPACE_DELETE))
        .then()
        .statusCode(204);
  }

  @Test
  public void testDeleteOneShouldFailWrongNamespace() {
    RestAssured
        .given()
        .when()
        .delete(getDefaultURI("kube-system2"))
        .then()
        .statusCode(404);
  }

  @Test
  public void testDeleteOneShouldFailUndefinedDeployment() {
    RestAssured
        .given()
        .when()
        .delete(getURI("deployment", NAMESPACE_DELETE))
        .then()
        .statusCode(400);
  }

  @Test
  public void testPostOneShouldFailWrongNamespace() {
    RestAssured
        .given()
        .when()
        .post(postURI("kube-system2"))
        .then()
        .statusCode(404);
  }

  @Test
  public void testPostOne() {
    ContainerDTO containerDTO = new ContainerDTO();
    containerDTO.setImage("nginx:1.7.9");
    containerDTO.setName("nginx");
    containerDTO.setPorts(new ArrayList<>());

    DeploymentDTO deploymentDTO = new DeploymentDTO();
    deploymentDTO.setName("nginx");
    deploymentDTO.setLabels(new HashMap<>());
    deploymentDTO.setReplicas(1);
    deploymentDTO.setContainer(containerDTO);

    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .body(deploymentDTO)
        .when()
        .post(postURI("test-delete"))
        .then()
        .statusCode(201);
  }

  private String getDefaultURI(final String namespace) {
    System.out.println(RestAssured.baseURI + DEFAULT_DEPLOYMENT + "?namespace=" + namespace);
    return DEFAULT_DEPLOYMENT + "?namespace=" + namespace;
  }

  private String getURI(final String deployment, final String namespace) {
    return deployment + "?namespace=" + namespace;
  }

  private String postURI(final String namespace) {
    return "?namespace=" + namespace;
  }
}
