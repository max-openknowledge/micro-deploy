package de.max.micro.deploy.application.replicaset;

import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.ws.rs.core.MediaType;

import static de.max.micro.deploy.infrastructure.constants.Constants.DEFAULT_REPLICASET;
import static de.max.micro.deploy.infrastructure.constants.Constants.NAMESPACE;
import static de.max.micro.deploy.infrastructure.constants.Constants.NAMESPACE_DELETE;
import static de.max.micro.deploy.infrastructure.constants.Constants.PATH_REPLICASET;
import static de.max.micro.deploy.infrastructure.constants.Constants.URI;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ReplicaSetResourceIT {

  @BeforeAll
  public void setUp() {
    RestAssured.baseURI = URI + PATH_REPLICASET;
  }

  @Test
  public void testGetReplicaset() {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getDefaultURI(NAMESPACE))
        .then()
        .statusCode(200);
  }

  @Test
  public void testGetReplicasetWrongNamespace() {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getDefaultURI("a"))
        .then()
        .statusCode(404);
  }

  @Test
  public void testGetReplicasetWrongReplicaset() {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getURI("a", NAMESPACE))
        .then()
        .statusCode(400);
  }

  @Test
  public void testGetAllReplicasets() {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getListURI(NAMESPACE))
        .then()
        .contentType(MediaType.APPLICATION_JSON)
        .body("size()", Matchers.is(2))
        .statusCode(200);
  }

  @Test
  public void testGetAllReplicasetsWrongNamespace() {
    RestAssured
        .given()
        .when()
        .get(getListURI("a"))
        .then()
        .statusCode(400);
  }

  @Test
  public void testDeleteOne() {
    RestAssured
        .given()
        .when()
        .delete(getDefaultURI(NAMESPACE_DELETE))
        .then()
        .statusCode(204);
  }

  @Test
  public void testDeleteOneWrongNamespace() {
    RestAssured
        .given()
        .when()
        .delete(getDefaultURI("a"))
        .then()
        .statusCode(404);
  }

  @Test
  public void testDeleteOneWrongReplicaset() {
    RestAssured
        .given()
        .when()
        .delete(getURI("a", NAMESPACE_DELETE))
        .then()
        .statusCode(400);
  }

  private String getURI(final String replicaset, final String namespace) {
    return "/" + replicaset + "?namespace=" + namespace;
  }

  private String getDefaultURI(final String namespace) {
    return DEFAULT_REPLICASET + "?namespace=" + namespace;
  }

  private String getListURI(final String namespace) {
    return "?namespace=" + namespace;
  }
}
