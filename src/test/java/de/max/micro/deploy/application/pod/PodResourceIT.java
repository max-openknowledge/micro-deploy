package de.max.micro.deploy.application.pod;

import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.ws.rs.core.MediaType;

import static de.max.micro.deploy.infrastructure.constants.Constants.DEFAULT_POD;
import static de.max.micro.deploy.infrastructure.constants.Constants.NAMESPACE;
import static de.max.micro.deploy.infrastructure.constants.Constants.NAMESPACE_DELETE;
import static de.max.micro.deploy.infrastructure.constants.Constants.PATH_POD;
import static de.max.micro.deploy.infrastructure.constants.Constants.URI;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PodResourceIT {

  @BeforeAll
  public void setUp() {
    RestAssured.baseURI = URI + PATH_POD;
  }

  @Test
  public void testGetPod() {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getDefaultURI(NAMESPACE))
        .then()
        .statusCode(200);
  }

  @Test
  public void testGetPodWrongNamespace() {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getDefaultURI("a"))
        .then()
        .statusCode(400);
  }

  @Test
  public void testGetAllPodsWrongNamespace() {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get("?namespace=1")
        .then()
        .statusCode(400);
  }

  @Test
  public void testGetAllPods() {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get("?namespace=" + NAMESPACE)
        .then()
        .contentType(MediaType.APPLICATION_JSON)
        .body("size()", Matchers.is(3))
        .statusCode(200);
  }

  @Test
  public void testDeleteOne() {
    RestAssured
        .given()
        .when()
        .delete(getDefaultURI(NAMESPACE_DELETE))
        .then()
        .statusCode(204);
  }

  @Test
  public void testDeleteOneFailWrongNamespace() {
    RestAssured
        .given()
        .when()
        .delete(getDefaultURI("a"))
        .then()
        .statusCode(400);
  }

  @Test
  public void testDeleteOneFailWrongPod() {
    RestAssured
        .given()
        .when()
        .delete(getURI("a", NAMESPACE))
        .then()
        .statusCode(404);
  }

  private String getURI(final String pod, final String namespace) {
    return "/" + pod + "?namespace=" + namespace;
  }

  private String getDefaultURI(final String namespace) {
    return DEFAULT_POD + "?namespace=" + namespace;
  }
}
