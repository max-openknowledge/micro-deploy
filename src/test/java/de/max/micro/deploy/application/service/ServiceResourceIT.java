package de.max.micro.deploy.application.service;

import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.MediaType;

import static de.max.micro.deploy.infrastructure.constants.Constants.NAMESPACE;
import static de.max.micro.deploy.infrastructure.constants.Constants.PATH_REPLICASET;
import static de.max.micro.deploy.infrastructure.constants.Constants.URI;

public class ServiceResourceIT {
  @Test
  public void testGetReplicaSet() {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(URI + PATH_REPLICASET + "?name=storage-provision&namespace=" + NAMESPACE)
        .then()
        .statusCode(200);
  }

  @Test
  public void testGetReplicaSetShouldFailWrongNamespace() {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(URI + PATH_REPLICASET + "?name=storage-provision&namespace=kube-system2")
        .then()
        .statusCode(400);
  }

  @Test
  public void testGetAllReplicaSets() {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(URI + PATH_REPLICASET + "?namespace=kube-system")
        .then()
        .contentType(MediaType.APPLICATION_JSON)
        .body("size()", Matchers.is(1))
        .statusCode(200);
  }

  @Test
  public void testGetAllReplicaSetstShouldFailWrongNamespace() {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(URI + PATH_REPLICASET + "?namespace=kube-system2")
        .then()
        .statusCode(400);
  }
}
