package de.max.micro.deploy.infrastructure.constants;

public class Constants {
  //public static final String URI = "http://192.168.64.9:31452";
  public static final String URI = "http://localhost:8080";

  public static final String NAMESPACE = "test";

  public static final String NAMESPACE_DELETE = "test-delete";

  public static final String PATH_DEPLOYMENT = "/api/deployment";

  public static final String PATH_POD = "/api/pod";

  public static final String PATH_REPLICASET = "/api/replicaset";

  public static final String PATH_SECRET = "/api/secret";

  public static final String PATH_SERVICE = "/api/service";

  public static final String DEFAULT_DEPLOYMENT = "/nginx-deployment";

  public static final String DEFAULT_POD = "/nginx-pod";

  public static final String DEFAULT_REPLICASET = "/nginx-replicaset";

  public static final String DEFAULT_SECRET = "/nginx-secret";
}
