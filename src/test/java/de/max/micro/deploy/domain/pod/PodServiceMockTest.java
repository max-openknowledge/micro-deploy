package de.max.micro.deploy.domain.pod;

import de.max.micro.deploy.domain.deployment.DeploymentService;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.NamespaceBuilder;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.PodBuilder;
import io.fabric8.kubernetes.api.model.PodListBuilder;
import io.fabric8.kubernetes.client.server.mock.KubernetesServer;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.migrationsupport.rules.EnableRuleMigrationSupport;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@EnableRuleMigrationSupport
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PodServiceMockTest {

  @Rule
  public KubernetesServer server = new KubernetesServer();

  @Test
  public void testListPods() {
    server
        .expect()
        .withPath("/api/v1/namespaces/test")
        .andReturn(200, new NamespaceBuilder()
            .withNewMetadata()
            .withName("test")
            .endMetadata()
            .build())
        .once();

    server
        .expect()
        .withPath("/api/v1/namespaces/test/pods")
        .andReturn(200, new PodListBuilder()
            .withItems(new PodBuilder()
                .withNewMetadata()
                .withName("test")
                .endMetadata()
                .build())
            .build())
        .once();

    PodService podService = new PodService(server.getClient());

    List<Pod> list = podService.listPods(podService.getNamespace("test"));

    assertThat(list.size()).isEqualTo(1);
    assertThat(list
        .get(0)
        .getMetadata()
        .getName()).isEqualTo("test");
  }

  @Test
  public void testGetPodByName() {
    server
        .expect()
        .withPath("/api/v1/namespaces/test")
        .andReturn(200, new NamespaceBuilder()
            .withNewMetadata()
            .withName("test")
            .endMetadata()
            .build())
        .once();

    server
        .expect()
        .withPath("/api/v1/namespaces/test/pods")
        .andReturn(200, new PodListBuilder()
            .withItems(new PodBuilder()
                .withNewMetadata()
                .withName("test")
                .endMetadata()
                .build())
            .build())
        .once();

    PodService podService = new PodService(server.getClient());
    Pod test = podService.getPodByName("test");

    assertThat(test
        .getMetadata()
        .getName()).isEqualTo("test");
  }

  @Test
  public void testDeletePod() {
    server
        .expect()
        .withPath("/api/v1/namespaces/test")
        .andReturn(200, new NamespaceBuilder()
            .withNewMetadata()
            .withName("test")
            .endMetadata()
            .build())
        .once();

    server
        .expect()
        .withPath("/api/v1/namespaces/test/pods/pod1")
        .andReturn(200, new PodBuilder().build())
        .once();

    PodService podService = new PodService(server.getClient());
    boolean delete = podService.deletePod(new PodBuilder()
        .withNewMetadata()
        .withName("pod1")
        .endMetadata()
        .build(), "test");

    assertThat(delete).isTrue();
  }

  @Test
  public void testGetNamespace() {
    server
        .expect()
        .withPath("/api/v1/namespaces/test")
        .andReturn(200, new NamespaceBuilder()
            .withNewMetadata()
            .withName("test")
            .endMetadata()
            .build())
        .once();

    DeploymentService deploymentService = new DeploymentService(server.getClient());

    Namespace namespace = deploymentService.getNamespace("test");

    assertThat(namespace
        .getMetadata()
        .getName()).isEqualTo("test");
  }
}
