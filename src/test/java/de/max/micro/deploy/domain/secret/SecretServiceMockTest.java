package de.max.micro.deploy.domain.secret;

import de.max.micro.deploy.application.secret.SecretDTO;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.NamespaceBuilder;
import io.fabric8.kubernetes.api.model.Secret;
import io.fabric8.kubernetes.api.model.SecretBuilder;
import io.fabric8.kubernetes.api.model.SecretListBuilder;
import io.fabric8.kubernetes.client.server.mock.KubernetesServer;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.migrationsupport.rules.EnableRuleMigrationSupport;

import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@EnableRuleMigrationSupport
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SecretServiceMockTest {

  @Rule
  public KubernetesServer server = new KubernetesServer();

  @Test
  public void testListSecrets() {
    server
        .expect()
        .withPath("/api/v1/namespaces/test")
        .andReturn(200, new NamespaceBuilder()
            .withNewMetadata()
            .withName("test")
            .endMetadata()
            .build())
        .once();

    server
        .expect()
        .withPath("/api/v1/namespaces/test/secrets")
        .andReturn(200, new SecretListBuilder().withItems(new SecretBuilder()
            .withData(new HashMap<String, String>() {{
              put("username", "test_name");
              put("password", "test_pass");
            }})
            .build()))
        .once();

    SecretService secretService = new SecretService(server.getClient());
    List<Secret> list = secretService.listSecrets(secretService.getNamespace("test"));

    assertThat(list.size()).isEqualTo(1);

    assertThat(list
        .get(0)
        .getData()
        .get("username")).isEqualTo("test_name");

    assertThat(list
        .get(0)
        .getData()
        .get("password")).isEqualTo("test_pass");
  }

  //@Test
  //public void getSecretByName() {
  //  server
  //      .expect()
  //      .withPath("/api/v1/namespaces/test")
  //      .andReturn(200, new SecretBuilder()
  //          .withNewMetadata()
  //          .withName("secret")
  //          .endMetadata()
  //          .build())
  //      .once();

  //  server
  //      .expect()
  //      .withPath("/api/v1/namespaces/test/secrets/test")
  //      .andReturn(200, new SecretBuilder().build())
  //      .once();

  //  SecretService secretService = new SecretService(server.getClient());
  //  Secret test = secretService.getSecretByName("test");

  //  assertThat(test
  //      .getMetadata()
  //      .getName()).isEqualTo("test");
  //}

  @Test
  public void testCreateSecret() {
    SecretDTO secretDTO = new SecretDTO();
    secretDTO.setName("test");
    secretDTO.setLabels(new HashMap<>());
    secretDTO.setData(new HashMap<String, String>() {{
      put("username", "test_name");
      put("password", "test_pass");
    }});

    SecretService secretService = new SecretService(server.getClient());
    Secret secret = secretService.createSecret(secretDTO);

    assertThat(secretDTO.getName()).isEqualTo(secret
        .getMetadata()
        .getName());

    assertThat(secretDTO
        .getData()
        .get("username")).isEqualTo(secret
        .getData()
        .get("username"));

    assertThat(secretDTO
        .getData()
        .get("password")).isEqualTo(secret
        .getData()
        .get("password"));
  }

  @Test
  public void deleteSecretTest() {
    Secret secret = new SecretBuilder()
        .withNewMetadata()
        .withName("secret")
        .endMetadata()
        .build();

    server
        .expect()
        .withPath("/api/v1/namespaces/test/secrets/secret")
        .andReturn(200, secret)
        .once();

    SecretService secretService = new SecretService(server.getClient());

    boolean delete = secretService.deleteSecret(secret, "test");

    assertThat(delete).isTrue();
  }

  @Test
  public void getNamespaceTest() {
    server
        .expect()
        .withPath("/api/v1/namespaces/test")
        .andReturn(200, new NamespaceBuilder()
            .withNewMetadata()
            .withName("test")
            .endMetadata()
            .build())
        .once();

    SecretService secretService = new SecretService(server.getClient());

    Namespace namespace = secretService.getNamespace("test");

    assertThat(namespace
        .getMetadata()
        .getName()).isEqualTo("test");
  }
}
