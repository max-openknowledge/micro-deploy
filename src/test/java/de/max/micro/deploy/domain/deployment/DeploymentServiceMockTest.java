package de.max.micro.deploy.domain.deployment;

import de.max.micro.deploy.application.deployment.ContainerDTO;
import de.max.micro.deploy.application.deployment.DeploymentDTO;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.NamespaceBuilder;
import io.fabric8.kubernetes.api.model.apps.Deployment;
import io.fabric8.kubernetes.api.model.apps.DeploymentBuilder;
import io.fabric8.kubernetes.api.model.apps.DeploymentListBuilder;
import io.fabric8.kubernetes.client.server.mock.KubernetesServer;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.migrationsupport.rules.EnableRuleMigrationSupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@EnableRuleMigrationSupport
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DeploymentServiceMockTest {

  @Rule
  public KubernetesServer server = new KubernetesServer();

  @Test
  public void listDeployments() {
    server
        .expect()
        .withPath("/api/v1/namespaces/test")
        .andReturn(200, new NamespaceBuilder()
            .withNewMetadata()
            .withName("test")
            .endMetadata()
            .build())
        .once();

    server
        .expect()
        .withPath("/apis/apps/v1/namespaces/test/deployments")
        .andReturn(200, new DeploymentListBuilder()
            .withItems(new DeploymentBuilder()
                .withNewMetadata()
                .withName("test-deployment")
                .endMetadata()
                .build())
            .build())
        .once();

    DeploymentService deploymentService = new DeploymentService(server.getClient());
    List<Deployment> list = deploymentService.listDeployments(deploymentService.getNamespace("test"));

    assertThat(list.size()).isEqualTo(1);
    assertThat(list
        .get(0)
        .getMetadata()
        .getName()).isEqualTo("test-deployment");
  }

  @Test
  public void createDeployment() {
    ContainerDTO containerDTO = new ContainerDTO();
    containerDTO.setImage("test");
    containerDTO.setName("test");
    containerDTO.setPorts(new ArrayList<>());

    DeploymentDTO deploymentDTO = new DeploymentDTO();
    deploymentDTO.setName("test");
    deploymentDTO.setLabels(new HashMap<>());
    deploymentDTO.setReplicas(0);
    deploymentDTO.setContainer(containerDTO);

    DeploymentService deploymentService = new DeploymentService(server.getClient());

    Deployment deployment = deploymentService.createDeployment(deploymentDTO);

    assertThat(deploymentDTO.getName()).isEqualTo(deployment
        .getMetadata()
        .getName());
    assertThat(deploymentDTO.getLabels()).isEqualTo(deployment
        .getMetadata()
        .getLabels());
    assertThat(deploymentDTO.getReplicas()).isEqualTo(deployment
        .getSpec()
        .getReplicas());
    assertThat(deploymentDTO
        .getContainer()
        .getName()).isEqualTo(deployment
        .getSpec()
        .getTemplate()
        .getSpec()
        .getContainers()
        .get(0)
        .getName());
    assertThat(deploymentDTO
        .getContainer()
        .getImage()).isEqualTo(deployment
        .getSpec()
        .getTemplate()
        .getSpec()
        .getContainers()
        .get(0)
        .getImage());
  }

  @Test
  public void getDeploymentByName() {
    server
        .expect()
        .withPath("/api/v1/namespaces/test")
        .andReturn(200, new NamespaceBuilder()
            .withNewMetadata()
            .withName("test")
            .endMetadata()
            .build())
        .once();

    server
        .expect()
        .withPath("/apis/apps/v1/namespaces/test/deployments")
        .andReturn(200, new DeploymentListBuilder()
            .withItems(new DeploymentBuilder()
                .withNewMetadata()
                .withName("test")
                .endMetadata()
                .build())
            .build())
        .once();

    DeploymentService deploymentService = new DeploymentService(server.getClient());
    Deployment test = deploymentService.getDeploymentByName("test", "test");

    assertThat(test
        .getMetadata()
        .getName()).isEqualTo("test");
  }

  @Test
  public void deleteDeployment() {
    Deployment deployment1 = new DeploymentBuilder()
        .withNewMetadata()
        .withName("deployment1")
        .endMetadata()
        .build();

    server
        .expect()
        .withPath("/apis/apps/v1/namespaces/test/deployments/deployment1")
        .andReturn(200, deployment1)
        .once();

    DeploymentService deploymentService = new DeploymentService(server.getClient());

    boolean delete = deploymentService.deleteDeployment(deployment1, "test");

    assertThat(delete).isTrue();
  }

  @Test
  public void getNamespaceTest() {
    server
        .expect()
        .withPath("/api/v1/namespaces/test")
        .andReturn(200, new NamespaceBuilder()
            .withNewMetadata()
            .withName("test")
            .endMetadata()
            .build())
        .once();

    DeploymentService deploymentService = new DeploymentService(server.getClient());

    Namespace namespace = deploymentService.getNamespace("test");

    assertThat(namespace
        .getMetadata()
        .getName()).isEqualTo("test");
  }
}
