package de.max.micro.deploy.domain.replicaset;

import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.NamespaceBuilder;
import io.fabric8.kubernetes.api.model.apps.ReplicaSet;
import io.fabric8.kubernetes.api.model.apps.ReplicaSetBuilder;
import io.fabric8.kubernetes.api.model.apps.ReplicaSetListBuilder;
import io.fabric8.kubernetes.client.server.mock.KubernetesServer;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.migrationsupport.rules.EnableRuleMigrationSupport;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@EnableRuleMigrationSupport
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ReplicaSetMockTest {
  @Rule
  public KubernetesServer server = new KubernetesServer();

  @Test
  public void testListReplicasets() {
    server
        .expect()
        .withPath("/api/v1/namespaces/test")
        .andReturn(200, new NamespaceBuilder()
            .withNewMetadata()
            .withName("test")
            .endMetadata()
            .build())
        .once();

    server
        .expect()
        .withPath("/apis/extensions/v1beta1/namespaces/test/replicasets")
        .andReturn(200, new ReplicaSetListBuilder()
            .withItems(new ReplicaSetBuilder()
                .withNewMetadata()
                .withName("test")
                .endMetadata()
                .build())
            .build())
        .once();

    ReplicasetService replicasetService = new ReplicasetService(server.getClient());

    List<ReplicaSet> list = replicasetService.listReplicasets(replicasetService.getNamespace("test"));

    assertThat(list.size()).isEqualTo(1);
    assertThat(list
        .get(0)
        .getMetadata()
        .getName()).isEqualTo("test");
  }

  @Test
  public void testGetReplicaSetByName() {
    server
        .expect()
        .withPath("/api/v1/namespaces/test")
        .andReturn(200, new NamespaceBuilder()
            .withNewMetadata()
            .withName("test")
            .endMetadata()
            .build())
        .once();

    server
        .expect()
        .withPath("/apis/extensions/v1beta1/namespaces/test/replicasets")
        .andReturn(200, new ReplicaSetListBuilder()
            .withItems(new ReplicaSetBuilder()
                .withNewMetadata()
                .withName("test")
                .endMetadata()
                .build())
            .build())
        .once();

    ReplicasetService replicasetService = new ReplicasetService(server.getClient());
    ReplicaSet test = replicasetService.getReplicaSetByName("test","test");

    assertThat(test
        .getMetadata()
        .getName()).isEqualTo("test");
  }

  @Test
  public void testDeleteReplicaset() {
    server
        .expect()
        .withPath("/apis/extensions/v1beta1/namespaces/test/replicasets/rs")
        .andReturn(200, new ReplicaSetBuilder().build())
        .once();

    ReplicasetService replicasetService = new ReplicasetService(server.getClient());
    boolean delete = replicasetService.deleteReplicaSet(new ReplicaSetBuilder()
        .withNewMetadata()
        .withName("rs")
        .endMetadata()
        .build(), "test");

    assertThat(delete).isTrue();
  }

  @Test
  public void testGetNameSpace() {
    server
        .expect()
        .withPath("/api/v1/namespaces/test")
        .andReturn(200, new NamespaceBuilder()
            .withNewMetadata()
            .withName("test")
            .endMetadata()
            .build())
        .once();

    ReplicasetService replicasetService = new ReplicasetService(server.getClient());

    Namespace namespace = replicasetService.getNamespace("test");

    assertThat(namespace
        .getMetadata()
        .getName()).isEqualTo("test");
  }
}
