package de.max.micro.deploy.domain.service;

import io.fabric8.kubernetes.api.model.NamespaceBuilder;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.ServiceBuilder;
import io.fabric8.kubernetes.api.model.ServiceListBuilder;
import io.fabric8.kubernetes.client.server.mock.KubernetesServer;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.migrationsupport.rules.EnableRuleMigrationSupport;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@EnableRuleMigrationSupport
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ServiceServiceMockTest {
  @Rule
  public KubernetesServer server = new KubernetesServer();

  @Test
  public void testListServices() {
    server
        .expect()
        .withPath("/api/v1/namespaces/test")
        .andReturn(200, new NamespaceBuilder()
            .withNewMetadata()
            .withName("test")
            .endMetadata()
            .build())
        .once();

    server
        .expect()
        .withPath("/api/v1/namespaces/test/services")
        .andReturn(200, new ServiceListBuilder()
            .withItems(new ServiceBuilder()
                .withNewMetadata()
                .withName("test")
                .endMetadata()
                .build())
            .build())
        .once();

    ServiceService svcService = new ServiceService(server.getClient());

    List<Service> list = svcService.listServices(svcService.getNamespace("test"));

    assertThat(list.size()).isEqualTo(1);
    assertThat(list
        .get(0)
        .getMetadata()
        .getName()).isEqualTo("test");
  }

  @Test
  public void testGetOneServiceByName() {
    server
        .expect()
        .withPath("/api/v1/namespaces/test")
        .andReturn(200, new NamespaceBuilder()
            .withNewMetadata()
            .withName("test")
            .endMetadata()
            .build())
        .once();

    server
        .expect()
        .withPath("/api/v1/namespaces/test/services")
        .andReturn(200, new ServiceListBuilder()
            .withItems(new ServiceBuilder()
                .withNewMetadata()
                .withName("test")
                .endMetadata()
                .build())
            .build())
        .once();

    ServiceService svcService = new ServiceService(server.getClient());

    Service test = svcService.getServiceByName("test");

    assertThat(test
        .getMetadata()
        .getName()).isEqualTo("test");
  }

  @Test
  public void testDeleteService() {
    server
        .expect()
        .withPath("/api/v1/namespaces/test/services/svc")
        .andReturn(200, new ServiceListBuilder()
            .withItems(new ServiceBuilder()
                .withNewMetadata()
                .withName("svc")
                .endMetadata()
                .build())
            .build())
        .once();

    ServiceService svcService = new ServiceService(server.getClient());

    boolean delete = svcService.deleteService(new ServiceBuilder()
        .withNewMetadata()
        .withName("svc")
        .endMetadata()
        .build(), "test");

    assertThat(delete).isTrue();

  }
}
