package de.max.micro.deploy.infrastructure.rest.error;

import javax.validation.Payload;

import static org.apache.commons.lang3.Validate.notNull;

public abstract class ValidationErrorPayload implements Payload {
  String errorCode;

  String message;

  public ValidationErrorPayload(final String errorCode, final String message) {
    this.errorCode = notNull(errorCode, "errorCode must not be null");
    this.message = notNull(message, "message must not be null");
  }

  public String getErrorCode() {
    return errorCode;
  }

  public String getMessage() {
    return message;
  }
}
