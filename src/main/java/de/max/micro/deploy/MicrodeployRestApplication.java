package de.max.micro.deploy;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 */
@ApplicationPath("/api")
@OpenAPIDefinition(info = @Info(title = "Deployment API",
    description = "Creates, Views and Deletes Kubernetes Deployments",
    version = "1.0-SNAPSHOT"))
public class MicrodeployRestApplication extends Application {
}
