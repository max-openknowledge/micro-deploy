package de.max.micro.deploy.application.service;

import de.max.micro.deploy.domain.namespace.NamespaceNotFoundException;
import de.max.micro.deploy.domain.service.ServiceNotFoundException;
import de.max.micro.deploy.domain.service.ServiceService;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.Service;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/service")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ServicesResource {

  private static final Logger LOG = LoggerFactory.getLogger(ServicesResource.class);

  @Inject
  ServiceService serviceService;

  @Operation(description = "Get a list of services from the given namespace")
  @APIResponses(value = { @APIResponse(responseCode = "200",
      description = "List of services",
      content = @Content(mediaType = "application/json")), @APIResponse(responseCode = "400",
      description = "Namespace not found") })
  @GET
  public Response getServices(@QueryParam("namespace") final String namespace) {
    try {
      Namespace ns = this.serviceService.getNamespace(namespace);
      List<Service> list = this.serviceService.listServices(ns);

      LOG.info("Found {} services in namespace: {}", list.size(), namespace);

      return Response
          .ok()
          .entity(list)
          .build();
    } catch (NamespaceNotFoundException n) {
      LOG.info("Could not find namespace: {}", n.getNamespace());
      return Response
          .status(Response.Status.BAD_REQUEST)
          .build();
    }
  }

  @Operation(description = "Remove the given service in the given namespace")
  @APIResponses(value = { @APIResponse(responseCode = "204",
      description = "Service removed"), @APIResponse(responseCode = "404",
      description = "Namespace not found"), @APIResponse(responseCode = "400",
      description = "Service not found"), })
  @DELETE
  @Path("{name}")
  public Response removeDeployment(@Parameter(description = "Name of the service to remove",
      required = true) @PathParam("name") final String serviceName, @QueryParam("namespace") final String namespace) {
    try {
      Namespace ns = this.serviceService.getNamespace(namespace);
      String nsName = ns
          .getMetadata()
          .getName();

      Service service = this.serviceService.getServiceByName(serviceName);
      this.serviceService.deleteService(service, nsName);

      LOG.info("Removed service: {}", service
          .getMetadata()
          .getName());

      return Response
          .status(Response.Status.NO_CONTENT)
          .build();
    } catch (NamespaceNotFoundException n) {
      LOG.info("Could not find namespace: {}", n.getNamespace());
      return Response
          .status(Response.Status.NOT_FOUND)
          .build();
    } catch (ServiceNotFoundException s) {
      LOG.info("Could not find service: {}", s.getService());
      return Response
          .status(Response.Status.BAD_REQUEST)
          .build();
    }
  }

  @Operation(description = "Get a service from the given namespace")
  @APIResponses(value = { @APIResponse(responseCode = "204",
      description = "Service found"), @APIResponse(responseCode = "404",
      description = "Namespace not found"), @APIResponse(responseCode = "400",
      description = "Service not found"), })
  @GET
  @Path("{name}")
  public Response getOneDeployment(@PathParam("name") final String serviceName, @QueryParam("namespace") final String namespace) {
    try {
      Namespace ns = this.serviceService.getNamespace(namespace);
      String nsName = ns
          .getMetadata()
          .getName();

      Service service = this.serviceService.getServiceByName(serviceName);

      return Response
          .ok()
          .entity(service)
          .build();
    } catch (NamespaceNotFoundException n) {
      LOG.info("Could not find namespace: {}", n.getNamespace());
      return Response
          .status(Response.Status.NOT_FOUND)
          .build();
    } catch (ServiceNotFoundException s) {
      LOG.info("Could not find service: {}", s.getService());
      return Response
          .status(Response.Status.BAD_REQUEST)
          .build();
    }
  }
}
