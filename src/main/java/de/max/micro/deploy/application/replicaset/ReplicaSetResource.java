package de.max.micro.deploy.application.replicaset;

import de.max.micro.deploy.domain.namespace.NamespaceNotFoundException;
import de.max.micro.deploy.domain.replicaset.ReplicaSetNotFoundException;
import de.max.micro.deploy.domain.replicaset.ReplicasetService;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.apps.ReplicaSet;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/replicaset")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ReplicaSetResource {
  private static final Logger LOG = LoggerFactory.getLogger(ReplicaSetResource.class);

  @Inject
  ReplicasetService replicasetService;

  @Operation(description = "Get a list of replicasets from the given namespace")
  @APIResponses(value = { @APIResponse(responseCode = "200",
      description = "List of replicasets",
      content = @Content(mediaType = "application/json")), @APIResponse(responseCode = "400",
      description = "Namespace not found") })
  @GET
  public Response getReplicasets(@QueryParam("namespace") final String namespace) {
    try {
      Namespace ns = this.replicasetService.getNamespace(namespace);
      List<ReplicaSet> list = this.replicasetService.listReplicasets(ns);

      LOG.info("Found {} replicasets in namespace: {}", list.size(), namespace);

      return Response
          .ok()
          .entity(list)
          .build();
    } catch (NamespaceNotFoundException n) {
      LOG.info("Could not find namespace: {}", n.getNamespace());
      return Response
          .status(Response.Status.BAD_REQUEST)
          .build();
    }
  }

  @Operation(description = "Remove the given replicaset in the given namespace")
  @APIResponses(value = { @APIResponse(responseCode = "204",
      description = "Replicaset removed"), @APIResponse(responseCode = "404",
      description = "Namespace not found"), @APIResponse(responseCode = "400",
      description = "Replicaset not found"), })
  @DELETE
  @Path("{name}")
  public Response removeReplicaset(@Parameter(description = "Name of the replicaset to remove",
      required = true) @PathParam("name") final String serviceName, @QueryParam("namespace") final String namespace) {
    try {
      Namespace ns = this.replicasetService.getNamespace(namespace);
      String nsName = ns
          .getMetadata()
          .getName();

      ReplicaSet replicaset = this.replicasetService.getReplicaSetByName(serviceName, nsName);
      this.replicasetService.deleteReplicaSet(replicaset, nsName);

      LOG.info("Removed replicaset: {}", replicaset
          .getMetadata()
          .getName());

      return Response
          .status(Response.Status.NO_CONTENT)
          .build();
    } catch (NamespaceNotFoundException n) {
      LOG.info("Could not find namespace: {}", n.getNamespace());
      return Response
          .status(Response.Status.NOT_FOUND)
          .build();
    } catch (ReplicaSetNotFoundException r) {
      LOG.info("Could not find replicaset: {}", r.getReplicaset());
      return Response
          .status(Response.Status.BAD_REQUEST)
          .build();
    }
  }

  @Operation(description = "Get a replicaset from the given namespace")
  @APIResponses(value = { @APIResponse(responseCode = "204",
      description = "ReplicaSet found"), @APIResponse(responseCode = "404",
      description = "Namespace not found"), @APIResponse(responseCode = "400",
      description = "ReplicaSet not found"), })
  @GET
  @Path("{name}")
  public Response getOneReplicaset(@PathParam("name") final String replicasetName, @QueryParam("namespace") final String namespace) {
    try {
      Namespace ns = this.replicasetService.getNamespace(namespace);
      String nsName = ns
          .getMetadata()
          .getName();

      ReplicaSet replicaset = this.replicasetService.getReplicaSetByName(replicasetName, nsName);

      return Response
          .ok()
          .entity(replicaset)
          .build();
    } catch (NamespaceNotFoundException n) {
      LOG.info("Could not find namespace: {}", n.getNamespace());
      return Response
          .status(Response.Status.NOT_FOUND)
          .build();
    } catch (ReplicaSetNotFoundException r) {
      LOG.info("Could not find replicaset: {}", r.getReplicaset());
      return Response
          .status(Response.Status.BAD_REQUEST)
          .build();
    }
  }
}
