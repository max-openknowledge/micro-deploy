package de.max.micro.deploy.application.secret;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.Map;

@Schema(name = "SecretDTO",
    description = "Description of a secret")
public class SecretDTO {
  private String name;

  private Map<String, String> labels;

  private Map<String, String> data;

  public SecretDTO() {
  }

  public SecretDTO(final String name, final Map<String, String> labels, final Map<String, String> data) {
    this.name = name;
    this.labels = labels;
    this.data = data;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public Map<String, String> getLabels() {
    return labels;
  }

  public void setLabels(final Map<String, String> labels) {
    this.labels = labels;
  }

  public Map<String, String> getData() {
    return data;
  }

  public void setData(final Map<String, String> data) {
    this.data = data;
  }
}
