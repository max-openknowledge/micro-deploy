package de.max.micro.deploy.application.deployment;

import de.max.micro.deploy.domain.deployment.DeploymentValidationErrorPayload;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Map;

@Schema(name = "DeploymentDTO",
    description = "Description of a deployment")
public class DeploymentDTO {

  @NotNull(payload = DeploymentValidationErrorPayload.DeploymentNameNull.class)
  @NotEmpty(payload = DeploymentValidationErrorPayload.DeploymentNameEmpty.class)
  @Size(max = 253,
      payload = DeploymentValidationErrorPayload.DeploymentNameSize.class)
  private String name;

  @NotNull(payload = DeploymentValidationErrorPayload.DeploymentLabelsNull.class)
  @Min(value = 1,
      payload = DeploymentValidationErrorPayload.DeploymentLabelsSize.class)
  private Map<String, String> labels;

  @NotNull(payload = DeploymentValidationErrorPayload.DeploymentReplicasNull.class)
  private Integer replicas;

  @NotNull(payload = DeploymentValidationErrorPayload.ContainerNull.class)
  private ContainerDTO containerDTO;

  public DeploymentDTO() {
  }

  public DeploymentDTO(final String name, final Map<String, String> labels, final int replicas, final ContainerDTO containerDTO) {
    this.name = name;
    this.labels = labels;
    this.replicas = replicas;
    this.containerDTO = containerDTO;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public Map<String, String> getLabels() {
    return labels;
  }

  public void setLabels(final Map<String, String> labels) {
    this.labels = labels;
  }

  public Integer getReplicas() {
    return replicas;
  }

  public void setReplicas(final Integer replicas) {
    this.replicas = replicas;
  }

  public ContainerDTO getContainer() {
    return containerDTO;
  }

  public void setContainer(final ContainerDTO containerDTO) {
    this.containerDTO = containerDTO;
  }

  @Override
  public String toString() {
    return "DeploymentDTO{" + "name='" + name + '\'' + ", labels=" + labels + ", replicas=" + replicas + ", containerDTO="
        + containerDTO.toString() + '}';
  }
}
