package de.max.micro.deploy.application.deployment;

import de.max.micro.deploy.domain.deployment.DeploymentValidationErrorPayload;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Schema(name = "ContainerDTO",
    description = "Information about the given container")
public class ContainerDTO {
  @NotNull(payload = DeploymentValidationErrorPayload.ContainerNameNull.class)
  @NotEmpty(payload = DeploymentValidationErrorPayload.ContainerEmpty.class)
  @Size(max = 253,
      payload = DeploymentValidationErrorPayload.ContainerNameSize.class)
  private String name;

  @NotNull
  @Size(max = 255,
      payload = DeploymentValidationErrorPayload.ContainerImageSize.class)
  private String image;

  private List<Integer> ports;

  public ContainerDTO() {
  }

  public ContainerDTO(final String name, final String image, final List<Integer> ports) {
    this.name = name;
    this.image = image;
    this.ports = ports;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getImage() {
    return image;
  }

  public void setImage(final String image) {
    this.image = image;
  }

  public List<Integer> getPorts() {
    return ports;
  }

  public void setPorts(final List<Integer> ports) {
    this.ports = ports;
  }

  @Override
  public String toString() {
    return "ContainerDTO{" + "name='" + name + '\'' + ", image='" + image + '\'' + ", ports=" + ports + '}';
  }
}
