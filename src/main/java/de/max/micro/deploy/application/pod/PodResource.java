package de.max.micro.deploy.application.pod;

import de.max.micro.deploy.domain.namespace.NamespaceNotFoundException;
import de.max.micro.deploy.domain.pod.PodNotFoundException;
import de.max.micro.deploy.domain.pod.PodService;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.Pod;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/pod")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PodResource {

  private static final Logger LOG = LoggerFactory.getLogger(PodResource.class);

  @Inject
  private PodService podService;

  @Operation(description = "Get a pod from the given namespace")
  @APIResponses(value = { @APIResponse(responseCode = "200",
      description = "Pod found"), @APIResponse(responseCode = "400",
      description = "Namespace does´nt exists"), @APIResponse(responseCode = "404",
      description = "Pod not found") })
  @GET
  @Path("{name}")
  public Response getPod(@PathParam("name") final String name, @QueryParam("namespace") final String namespace) {
    try {
      Namespace ns = podService.getNamespace(namespace);

      Pod pod = podService.getPodByName(name);

      LOG.info("Found pod: {}", pod
          .getMetadata()
          .getName());
      return Response
          .ok()
          .entity(pod)
          .build();
    } catch (NamespaceNotFoundException n) {
      LOG.info("Could not find namespace: {}", n.getNamespace());
      return Response
          .status(Response.Status.BAD_REQUEST)
          .build();
    } catch (PodNotFoundException p) {
      LOG.info("Could not find pod: {}", p.getPod());
      return Response
          .status(Response.Status.NOT_FOUND)
          .build();
    }
  }

  @Operation(description = "Get a list of pods from the given namespace")
  @APIResponses(value = { @APIResponse(responseCode = "200",
      description = "List of pods",
      content = @Content(mediaType = "application/json")), @APIResponse(responseCode = "400",
      description = "Namespace not found") })
  @GET
  public Response getPodList(@QueryParam("namespace") final String namespace) {
    try {
      Namespace ns = this.podService.getNamespace(namespace);

      List<Pod> podList = this.podService.listPods(ns);

      LOG.info("Found {} pods in namespace: {}", podList.size(), namespace);

      return Response
          .ok()
          .entity(podList)
          .build();
    } catch (NamespaceNotFoundException n) {
      LOG.info("Could not find namespace: {}", n.getNamespace());
      return Response
          .status(Response.Status.BAD_REQUEST)
          .build();
    }
  }

  @Operation(description = "Remove a pod from the given namespace")
  @APIResponses(value = { @APIResponse(responseCode = "204",
      description = "Pod removed"), @APIResponse(responseCode = "400",
      description = "Namespace not found"), @APIResponse(responseCode = "404",
      description = "Pod not found"), @APIResponse(responseCode = "500",
      description = "Pod could not be removed"), })
  @DELETE
  @Path("{name}")
  public Response deletePod(@PathParam("name") final String name, @QueryParam("namespace") final String namespace) {
    if (name.isEmpty()) {
      LOG.info("Pod can't be empty");
      return Response
          .status(Response.Status.NOT_FOUND)
          .build();
    } else if (namespace.isEmpty()) {
      LOG.info("Namespace can't be empty");
      return Response
          .status(Response.Status.BAD_REQUEST)
          .build();
    }

    try {
      Namespace ns = this.podService.getNamespace(namespace);
      Pod pod = podService.getPodByName(name);

      this.podService.deletePod(pod, ns
          .getMetadata()
          .getName());

      return Response
          .status(Response.Status.NO_CONTENT)
          .build();
    } catch (NamespaceNotFoundException n) {
      LOG.info("Could not find namespace: {}", n.getNamespace());
      return Response
          .status(Response.Status.BAD_REQUEST)
          .build();
    } catch (PodNotFoundException p) {
      LOG.info("Could not find pod: {}", p.getPod());
      return Response
          .status(Response.Status.NOT_FOUND)
          .build();
    }
  }
}
