package de.max.micro.deploy.application.secret;

import de.max.micro.deploy.domain.namespace.NamespaceNotFoundException;
import de.max.micro.deploy.domain.secret.SecretNotFoundException;
import de.max.micro.deploy.domain.secret.SecretService;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.Secret;
import io.fabric8.kubernetes.client.KubernetesClientException;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/secret")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SecretResource {

  private static final Logger LOG = LoggerFactory.getLogger(SecretResource.class);

  @Inject
  SecretService secretService;

  @Operation(description = "Get a list of secrets from the given namespace")
  @APIResponses(value = { @APIResponse(responseCode = "200",
      description = "List of secrets",
      content = @Content(mediaType = "application/json")), @APIResponse(responseCode = "400",
      description = "Namespace not found") })
  @GET
  public Response getSecrets(@QueryParam("namespace") final String namespace) {
    try {
      Namespace ns = this.secretService.getNamespace(namespace);
      List<Secret> list = this.secretService.listSecrets(ns);

      LOG.info("Found {} secrets in namespace: {}", list.size(), namespace);

      return Response
          .ok()
          .entity(list)
          .build();
    } catch (NamespaceNotFoundException n) {
      LOG.info("Could not find namespace: {}", n.getNamespace());
      return Response
          .status(Response.Status.BAD_REQUEST)
          .build();
    }
  }

  @Operation(description = "Get a secret from the given namespace")
  @APIResponses(value = { @APIResponse(responseCode = "204",
      description = "Secret found"), @APIResponse(responseCode = "404",
      description = "Namespace not found"), @APIResponse(responseCode = "400",
      description = "Secret not found"), })
  @GET
  @Path("{name}")
  public Response getOneSecret(@PathParam("name") final String secretName, @QueryParam("namespace") final String namespace) {
    try {
      Namespace ns = this.secretService.getNamespace(namespace);

      Secret secret = this.secretService.getSecretByName(secretName, ns);

      LOG.info("Found secret: {} in namespace: {}", secret
          .getMetadata()
          .getName(), namespace);

      return Response
          .ok()
          .entity(secret)
          .build();
    } catch (NamespaceNotFoundException n) {
      LOG.info("Could not find namespace: {}", n.getNamespace());
      return Response
          .status(Response.Status.NOT_FOUND)
          .build();
    } catch (SecretNotFoundException s) {
      LOG.info("Could not find secret: {}", s.getSecret());
      return Response
          .status(Response.Status.BAD_REQUEST)
          .build();
    }
  }

  @Operation(description = "Creates a secret in the given namespace")
  @APIResponses(value = { @APIResponse(responseCode = "201",
      description = "Secret created"), @APIResponse(responseCode = "400",
      description = "Namespace not found") })
  @Parameter(name = "body",
      description = "Manifest of the secret",
      schema = @Schema(implementation = SecretDTO.class))
  @POST
  public Response createSecret(@RequestBody(required = true) final SecretDTO baseSecret,
      @Parameter(description = "Name of the secret to remove",
          required = true) @QueryParam("namespace") final String namespace) {
    try {
      Namespace ns = this.secretService.getNamespace(namespace);
      Secret secret = this.secretService.createSecret(baseSecret);

      secret = this.secretService.applySecret(secret, ns);

      LOG.info("Created new secret: {}", secret
          .getMetadata()
          .getName());

      return Response
          .status(Response.Status.CREATED)
          .build();

    } catch (NamespaceNotFoundException n) {
      LOG.info("Could not find namespace: {}", n.getNamespace());
      return Response
          .status(Response.Status.NOT_FOUND)
          .build();
    } catch (KubernetesClientException k) {
      if (k.getCode() == 409) {
        LOG.info("Secret {} already exists", baseSecret.getName());
        return Response
            .status(Response.Status.CONFLICT)
            .build();
      }
      LOG.info("Could not create Secret {}", baseSecret.getName());
      return Response
          .status(Response.Status.INTERNAL_SERVER_ERROR)
          .build();
    }
  }

  @Operation(description = "Remove the given secret in the given namespace")
  @APIResponses(value = { @APIResponse(responseCode = "204",
      description = "Secret removed"), @APIResponse(responseCode = "404",
      description = "Namespace not found"), @APIResponse(responseCode = "400",
      description = "Secret not found"), })
  @DELETE
  @Path("{name}")
  public Response removeSecret(@Parameter(description = "Name of the deployment to remove",
      required = true) @PathParam("name") final String secretName, @QueryParam("namespace") final String namespace) {
    try {
      Namespace ns = this.secretService.getNamespace(namespace);
      String nsName = ns
          .getMetadata()
          .getName();

      Secret secret = this.secretService.getSecretByName(secretName, ns);

      this.secretService.deleteSecret(secret, nsName);

      LOG.info("Removed secret: {}", secret
          .getMetadata()
          .getName());

      return Response
          .status(Response.Status.NO_CONTENT)
          .build();
    } catch (NamespaceNotFoundException n) {
      LOG.info("Could not find namespace: {}", n.getNamespace());
      return Response
          .status(Response.Status.NOT_FOUND)
          .build();
    } catch (SecretNotFoundException s) {
      LOG.info("Could not find deployment: {}", s.getSecret());
      return Response
          .status(Response.Status.BAD_REQUEST)
          .build();
    }
  }
}
