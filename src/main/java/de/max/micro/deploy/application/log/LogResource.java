package de.max.micro.deploy.application.log;

import de.max.micro.deploy.domain.log.LogService;
import de.max.micro.deploy.domain.namespace.NamespaceNotFoundException;
import de.max.micro.deploy.domain.pod.PodService;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.Pod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/log")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LogResource {

  private static final Logger LOG = LoggerFactory.getLogger(LogResource.class);

  @Inject
  LogService logService;

  @Inject
  PodService podService;

  @GET
  @Path("{name}")
  public Response getLogs(@PathParam("name") final String podName, @QueryParam("namespace") final String namespace) {
    try {
      Namespace ns = logService.getNamespace(namespace);

      Pod pod = podService.getPodByName(podName);

      String logs = logService.getLogs(pod, ns);

      return Response
          .ok()
          .entity(logs)
          .build();
    } catch (NamespaceNotFoundException n) {
      LOG.info("Could not find namespace: {}", n.getNamespace());
      return Response
          .status(Response.Status.NOT_FOUND)
          .build();
    }
  }
}
