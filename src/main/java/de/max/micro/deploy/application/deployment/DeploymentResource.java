package de.max.micro.deploy.application.deployment;

import de.max.micro.deploy.domain.deployment.DeploymentNotFoundException;
import de.max.micro.deploy.domain.deployment.DeploymentService;
import de.max.micro.deploy.domain.namespace.NamespaceNotFoundException;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.apps.Deployment;
import io.fabric8.kubernetes.client.KubernetesClientException;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/deployment")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DeploymentResource {

  private static final Logger LOG = LoggerFactory.getLogger(DeploymentResource.class);

  @Inject
  DeploymentService deploymentService;

  @Operation(description = "Get a list of deployments from the given namespace")
  @APIResponses(value = { @APIResponse(responseCode = "200",
      description = "List of deployments",
      content = @Content(mediaType = "application/json")), @APIResponse(responseCode = "400",
      description = "Namespace not found") })
  @GET
  public Response getDeployments(@Parameter(description = "Name of the deployment to remove",
      required = true,
      example = "nginx-deployment") @QueryParam("namespace") final String namespace) {
    try {
      Namespace ns = this.deploymentService.getNamespace(namespace);
      List<Deployment> list = this.deploymentService.listDeployments(ns);

      LOG.info("Found {} deployments in namespace: {}", list.size(), namespace);

      return Response
          .ok()
          .entity(list)
          .build();
    } catch (NamespaceNotFoundException n) {
      LOG.info("Could not find namespace: {}", n.getNamespace());
      return Response
          .status(Response.Status.BAD_REQUEST)
          .build();
    }
  }

  @Operation(description = "Get a deployment from the given namespace")
  @APIResponses(value = { @APIResponse(responseCode = "204",
      description = "Deployment found"), @APIResponse(responseCode = "404",
      description = "Namespace not found"), @APIResponse(responseCode = "400",
      description = "Deployment not found") })
  @GET
  @Path("{name}")
  public Response getOneDeployment(@PathParam("name") final String deploymentName, @QueryParam("namespace") final String namespace) {
    try {
      Namespace ns = this.deploymentService.getNamespace(namespace);
      String nsName = ns
          .getMetadata()
          .getName();

      Deployment deployment = this.deploymentService.getDeploymentByName(deploymentName, nsName);

      return Response
          .ok()
          .entity(deployment)
          .build();
    } catch (NamespaceNotFoundException n) {
      LOG.info("Could not find namespace: {}", n.getNamespace());
      return Response
          .status(Response.Status.NOT_FOUND)
          .build();
    } catch (DeploymentNotFoundException d) {
      LOG.info("Could not find deployment: {}", d.getDeployment());
      return Response
          .status(Response.Status.BAD_REQUEST)
          .build();
    }
  }

  @Operation(description = "Remove the given deployment in the given namespace")
  @APIResponses(value = { @APIResponse(responseCode = "204",
      description = "Deployment removed"), @APIResponse(responseCode = "404",
      description = "Namespace not found"), @APIResponse(responseCode = "400",
      description = "Deployment not found") })
  @DELETE
  @Path("{name}")
  public Response removeDeployment(@Parameter(description = "Name of the deployment to remove",
      required = true,
      example = "nginx-deployment") @PathParam("name") final String deploymentName, @QueryParam("namespace") final String namespace) {
    try {
      LOG.info("Try to delete deployment {} in namespace {}", deploymentName, namespace);
      Namespace ns = this.deploymentService.getNamespace(namespace);
      String nsName = ns
          .getMetadata()
          .getName();

      Deployment deployment = this.deploymentService.getDeploymentByName(deploymentName, nsName);

      this.deploymentService.deleteDeployment(deployment, nsName);

      LOG.info("Removed deployment: {}", deployment
          .getMetadata()
          .getName());

      return Response
          .status(Response.Status.NO_CONTENT)
          .build();
    } catch (NamespaceNotFoundException n) {
      LOG.info("Could not find namespace: {}", n.getNamespace());
      return Response
          .status(Response.Status.NOT_FOUND)
          .build();
    } catch (DeploymentNotFoundException d) {
      LOG.info("Could not find deployment: {}", d.getDeployment());
      return Response
          .status(Response.Status.BAD_REQUEST)
          .build();
    }
  }

  @Operation(description = "Creates a deployment in the given namespace")
  @APIResponses(value = { @APIResponse(responseCode = "201",
      description = "Deployment created"), @APIResponse(responseCode = "400",
      description = "Namespace not found"), @APIResponse(responseCode = "409",
      description = "Deployment already exists") })
  @Parameter(name = "body",
      description = "Manifest of the deployment",
      schema = @Schema(implementation = DeploymentDTO.class))
  @POST
  public Response createDeployment(@RequestBody(required = true) final DeploymentDTO baseDeploy,
      @Parameter(description = "Name of the deployment to remove",
          required = true,
          example = "nginx-deployment") @QueryParam("namespace") final String namespace) {
    try {
      LOG.debug("Create deployment with name: {} in namespace: {}", baseDeploy.getName(), namespace);

      Namespace ns = this.deploymentService.getNamespace(namespace);
      Deployment deployment = this.deploymentService.createDeployment(baseDeploy);

      deployment = this.deploymentService.applyDeployment(deployment, ns);

      LOG.info("Created new deployment: {}", deployment
          .getMetadata()
          .getName());

      return Response
          .status(Response.Status.CREATED)
          .build();

    } catch (NamespaceNotFoundException n) {
      LOG.info("Could not find namespace: {}", n.getNamespace());
      return Response
          .status(Response.Status.NOT_FOUND)
          .build();
    } catch (KubernetesClientException k) {
      if (k.getCode() == 409) {
        LOG.info("Deployment {} already exists", baseDeploy.getName());
        return Response
            .status(Response.Status.CONFLICT)
            .build();
      }
      LOG.info("Could not create Deployment {}", baseDeploy.getName());
      return Response
          .status(Response.Status.INTERNAL_SERVER_ERROR)
          .build();
    }
  }
}
