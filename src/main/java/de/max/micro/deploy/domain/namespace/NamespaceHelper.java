package de.max.micro.deploy.domain.namespace;

import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.client.KubernetesClient;

public class NamespaceHelper {
  public static Namespace getNamespace(final KubernetesClient client, final String namespace) {
    try {
      Namespace ns = client
          .namespaces()
          .withName(namespace)
          .get();

      if (ns == null) {
        throw new NamespaceNotFoundException(namespace);
      }

      return ns;
    } catch (Exception e) {
      throw new NamespaceNotFoundException(namespace);
    }
  }
}
