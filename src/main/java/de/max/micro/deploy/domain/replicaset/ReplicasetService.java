package de.max.micro.deploy.domain.replicaset;

import de.max.micro.deploy.domain.namespace.NamespaceHelper;
import de.max.micro.deploy.infrastructure.stereotypes.Service;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.apps.ReplicaSet;
import io.fabric8.kubernetes.api.model.apps.ReplicaSetList;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;

import java.util.List;
import java.util.Optional;

@Service
public class ReplicasetService {
  private KubernetesClient client;

  public ReplicasetService() {
    this.client = new DefaultKubernetesClient();
  }

  public ReplicasetService(final KubernetesClient client) {
    this.client = client;
  }

  public List<ReplicaSet> listReplicasets(final Namespace namespace) {
    return client
        .apps()
        .replicaSets()
        .inNamespace(namespace
            .getMetadata()
            .getName())
        .list()
        .getItems();
  }

  public ReplicaSet getReplicaSetByName(final String replicaSetName, final String namespace) {
    ReplicaSetList replicaSetList = client
        .apps()
        .replicaSets()
        .inNamespace(namespace)
        .list();

    Optional<ReplicaSet> r = replicaSetList
        .getItems()
        .stream()
        .filter(replicaset -> replicaset
            .getMetadata()
            .getName()
            .equals(replicaSetName))
        .findFirst();

    if (!r.isPresent()) {
      throw new ReplicaSetNotFoundException(replicaSetName);
    }

    return r.get();
  }

  public boolean deleteReplicaSet(final ReplicaSet replicaSet, final String namespace) {
    return client
        .apps()
        .replicaSets()
        .inNamespace(namespace)
        .delete(replicaSet);
  }

  public Namespace getNamespace(final String namespace) {
    return NamespaceHelper.getNamespace(client, namespace);
  }
}
