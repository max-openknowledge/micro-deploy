package de.max.micro.deploy.domain.deployment;

import de.max.micro.deploy.infrastructure.rest.error.ValidationErrorPayload;

public class DeploymentValidationErrorPayload {

  public static class ContainerNameNull extends ValidationErrorPayload {
    public ContainerNameNull(final String errorCode, final String message) {
      super("CONTAINER_NAME_NULL", "container name cannot be null");
    }
  }

  public static class ContainerEmpty extends ValidationErrorPayload {
    public ContainerEmpty(final String errorCode, final String message) {
      super("CONTAINER_NAME_EMPTY", "container name cannot be empty");
    }
  }

  public static class ContainerNameSize extends ValidationErrorPayload {
    public ContainerNameSize(final String errorCode, final String message) {
      super("CONTAINER_NAME_SIZE", "name must be between 0 and 253");
    }
  }

  public static class ContainerImageSize extends ValidationErrorPayload {
    public ContainerImageSize(final String errorCode, final String message) {
      super("CONTAINER_IMAGE_SIZE", "image must be between 0 and 255");
    }
  }

  public static class DeploymentNameNull extends ValidationErrorPayload {
    public DeploymentNameNull(final String errorCode, final String message) {
      super("DEPLOYMENT_NAME_NULL", "name cannot be null");
    }
  }

  public static class DeploymentNameEmpty extends ValidationErrorPayload {
    public DeploymentNameEmpty(final String errorCode, final String message) {
      super("DEPLOYMENT_NAME_EMPTY", "name cannot be empty");
    }
  }

  public static class DeploymentNameSize extends ValidationErrorPayload {
    public DeploymentNameSize(final String errorCode, final String message) {
      super("DEPLOYMENT_NAME_SIZE", "name size must be between 0 and 253");
    }
  }

  public static class DeploymentLabelsNull extends ValidationErrorPayload {
    public DeploymentLabelsNull(final String errorCode, final String message) {
      super("DEPLOYMENT_LABEL_NULL", "labels cannot be null");
    }
  }

  public static class DeploymentLabelsSize extends ValidationErrorPayload {
    public DeploymentLabelsSize(final String errorCode, final String message) {
      super("DEPLOYMENT_LABEL_SIZE", "labels must be at least size 1");
    }
  }

  public static class DeploymentReplicasNull extends ValidationErrorPayload {
    public DeploymentReplicasNull(final String errorCode, final String message) {
      super("DEPLOYMENT_REPLICAS_NULL", "replica cannot be null");
    }
  }

  public static class ContainerNull extends ValidationErrorPayload {
    public ContainerNull(final String errorCode, final String message) {
      super("CONTAINER_NULL", "container cannot be null");
    }
  }
}
