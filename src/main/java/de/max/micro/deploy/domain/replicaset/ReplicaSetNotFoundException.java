package de.max.micro.deploy.domain.replicaset;

import javax.ws.rs.NotFoundException;

public class ReplicaSetNotFoundException extends NotFoundException {
  private String replicaset;

  public ReplicaSetNotFoundException(final String replicaSet) {
    super();
    this.replicaset = replicaSet;
  }

  public ReplicaSetNotFoundException(final Throwable cause, final String replicaSet) {
    super(cause);
    this.replicaset = replicaSet;
  }

  @Override
  public String getMessage() {
    return String.format("Replicaset %s was not found", this.replicaset);
  }

  public String getReplicaset() {
    return replicaset;
  }
}
