package de.max.micro.deploy.domain.pod;

import javax.ws.rs.NotFoundException;

public class PodNotFoundException extends NotFoundException {
  private String pod;

  public PodNotFoundException(final String pod) {
    super();
    this.pod = pod;
  }

  public PodNotFoundException(final String pod, final Throwable cause) {
    super(cause);
    this.pod = pod;
  }

  @Override
  public String getMessage() {
    return String.format("Pod %s was not found", this.pod);
  }

  public String getPod() {
    return this.pod;
  }
}
