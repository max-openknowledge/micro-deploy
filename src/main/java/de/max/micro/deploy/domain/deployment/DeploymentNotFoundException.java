package de.max.micro.deploy.domain.deployment;

import javax.ws.rs.NotFoundException;

public class DeploymentNotFoundException extends NotFoundException {
  private final String deployment;

  public DeploymentNotFoundException(final String deployment) {
    super();
    this.deployment = deployment;
  }

  public DeploymentNotFoundException(final String deployment, final Throwable cause) {
    super(cause);
    this.deployment = deployment;
  }

  @Override
  public String getMessage() {
    return String.format("Deployment %s was not found", this.deployment);
  }

  public String getDeployment() {
    return this.deployment;
  }
}
