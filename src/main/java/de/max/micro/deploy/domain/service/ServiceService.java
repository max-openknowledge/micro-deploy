package de.max.micro.deploy.domain.service;

import de.max.micro.deploy.domain.namespace.NamespaceHelper;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.Service;
import io.fabric8.kubernetes.api.model.ServiceList;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;

import java.util.List;
import java.util.Optional;

@de.max.micro.deploy.infrastructure.stereotypes.Service
public class ServiceService {

  private KubernetesClient client;

  public ServiceService() {
    this.client = new DefaultKubernetesClient();
  }

  public ServiceService(final KubernetesClient client) {
    this.client = client;
  }

  public List<Service> listServices(final Namespace namespace) {
    return client
        .services()
        .inNamespace(namespace
            .getMetadata()
            .getName())
        .list()
        .getItems();
  }

  public Service getServiceByName(final String serviceName) {
    ServiceList list = client
        .services()
        .list();

    Optional<Service> s = list
        .getItems()
        .stream()
        .filter(svc -> svc
            .getMetadata()
            .getName()
            .equals(serviceName))
        .findFirst();

    if (!s.isPresent()) {
      throw new ServiceNotFoundException(serviceName);
    }

    return s.get();
  }

  public boolean deleteService(final Service service, final String namespace) {
    return this.client
        .services()
        .inNamespace(namespace)
        .delete(service);
  }

  public Namespace getNamespace(final String namespace) {
    return NamespaceHelper.getNamespace(client, namespace);
  }
}
