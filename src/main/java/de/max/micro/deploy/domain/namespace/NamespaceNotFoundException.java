package de.max.micro.deploy.domain.namespace;

import javax.ws.rs.NotFoundException;

public class NamespaceNotFoundException extends NotFoundException {

  private final String namespace;

  public NamespaceNotFoundException(final String namespace) {
    super();
    this.namespace = namespace;
  }

  public NamespaceNotFoundException(final String namespace, final Throwable cause) {
    super(cause);
    this.namespace = namespace;
  }

  @Override
  public String getMessage() {
    return String.format("Namespace %s was not found", this.namespace);
  }

  public String getNamespace() {
    return namespace;
  }
}
