package de.max.micro.deploy.domain.deployment;

import de.max.micro.deploy.application.deployment.ContainerDTO;
import de.max.micro.deploy.application.deployment.DeploymentDTO;
import de.max.micro.deploy.domain.namespace.NamespaceHelper;
import de.max.micro.deploy.infrastructure.stereotypes.Service;
import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.ContainerBuilder;
import io.fabric8.kubernetes.api.model.ContainerPortBuilder;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.apps.Deployment;
import io.fabric8.kubernetes.api.model.apps.DeploymentBuilder;
import io.fabric8.kubernetes.api.model.apps.DeploymentList;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DeploymentService {

  private KubernetesClient client;

  public DeploymentService() {
    this.client = new DefaultKubernetesClient();
  }

  public DeploymentService(final KubernetesClient client) {
    this.client = client;
  }

  public List<Deployment> listDeployments(final Namespace namespace) {
    return client
        .apps()
        .deployments()
        .inNamespace(namespace
            .getMetadata()
            .getName())
        .list()
        .getItems();
  }

  public Deployment getDeploymentByName(final String deployment, final String namespace) {
    DeploymentList deployments = client
        .apps()
        .deployments()
        .inNamespace(namespace)
        .list();

    Optional<Deployment> d = deployments
        .getItems()
        .stream()
        .filter(deploy -> deploy
            .getMetadata()
            .getName()
            .equals(deployment))
        .findFirst();

    if (!d.isPresent()) {
      throw new DeploymentNotFoundException(deployment);
    }
    return d.get();
  }

  public Deployment createDeployment(final DeploymentDTO deployDTO) {
    return new DeploymentBuilder()
        .withNewMetadata()
        .withName(deployDTO.getName())
        .addToLabels(deployDTO.getLabels())
        .endMetadata()
        .withNewSpec()
        .withReplicas(deployDTO.getReplicas())
        .withNewSelector()
        .withMatchLabels(deployDTO.getLabels())
        .endSelector()
        .withNewTemplate()
        .withNewMetadata()
        .withLabels(deployDTO.getLabels())
        .endMetadata()
        .withNewSpec()
        .addToContainers(createContainer(deployDTO))
        .endSpec()
        .endTemplate()
        .endSpec()
        .build();

  }

  public Deployment applyDeployment(final Deployment deployment, final Namespace namespace) {
    return client
        .apps()
        .deployments()
        .inNamespace(namespace
            .getMetadata()
            .getName())
        .create(deployment);
  }

  public boolean deleteDeployment(final Deployment deployment, final String namespace) {
    return client
        .apps()
        .deployments()
        .inNamespace(namespace)
        .delete(deployment);
  }

  public Namespace getNamespace(final String namespace) {
    return NamespaceHelper.getNamespace(client, namespace);
  }

  private Container createContainer(final DeploymentDTO deployDTO) {
    ContainerDTO containerDTO = deployDTO.getContainer();
    return new ContainerBuilder()
        .withName(containerDTO.getName())
        .withImage(containerDTO.getImage())
        .withPorts(containerDTO
            .getPorts()
            .stream()
            .map(integer -> new ContainerPortBuilder()
                .withContainerPort(integer)
                .build())
            .collect(Collectors.toList()))
        .build();
  }

}
