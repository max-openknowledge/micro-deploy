package de.max.micro.deploy.domain.pod;

import de.max.micro.deploy.domain.namespace.NamespaceHelper;
import de.max.micro.deploy.infrastructure.stereotypes.Service;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.PodList;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;

import java.util.List;
import java.util.Optional;

@Service
public class PodService {

  private KubernetesClient client;

  public PodService() {
    this.client = new DefaultKubernetesClient();
  }

  public PodService(final KubernetesClient client) {
    this.client = client;
  }

  public List<Pod> listPods(final Namespace namespace) {
    return client
        .pods()
        .inNamespace(namespace
            .getMetadata()
            .getName())
        .list()
        .getItems();
  }

  public Pod getPodByName(final String podName) {
    PodList podList = client
        .pods()
        .list();

    Optional<Pod> p = podList
        .getItems()
        .stream()
        .filter(pod -> pod
            .getMetadata()
            .getName()
            .equals(podName))
        .findFirst();

    if (!p.isPresent()) {
      throw new PodNotFoundException(podName);
    }
    return p.get();
  }

  public boolean deletePod(final Pod pod, final String namespace) {
    return client
        .pods()
        .inNamespace(namespace)
        .delete(pod);
  }

  public Namespace getNamespace(final String namespace) {
    return NamespaceHelper.getNamespace(client, namespace);
  }
}
