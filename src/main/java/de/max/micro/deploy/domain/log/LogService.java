package de.max.micro.deploy.domain.log;

import de.max.micro.deploy.domain.namespace.NamespaceHelper;
import de.max.micro.deploy.infrastructure.stereotypes.Service;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.dsl.LogWatch;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

@Service
public class LogService {

  private KubernetesClient client;

  public LogService() {
    this.client = new DefaultKubernetesClient();
  }

  public String getLogs(final Pod pod, final Namespace namespace) {
    LogWatch watch = client
        .pods()
        .inNamespace(namespace
            .getMetadata()
            .getName())
        .withName(pod
            .getMetadata()
            .getName())
        .tailingLines(10)
        .watchLog(System.out);

    return watchToString(watch);
  }

  private String watchToString(final LogWatch watch) {
    return new BufferedReader(new InputStreamReader(watch.getOutput()))
        .lines()
        .collect(Collectors.joining("\n"));
  }

  public Namespace getNamespace(final String namespace) {
    return NamespaceHelper.getNamespace(client, namespace);
  }
}
