package de.max.micro.deploy.domain.secret;

import javax.ws.rs.NotFoundException;

public class SecretNotFoundException extends NotFoundException {

  private String secret;

  public SecretNotFoundException(final String secret) {
    super();
    this.secret = secret;
  }

  public SecretNotFoundException(final Throwable cause, final String secret) {
    super(cause);
    this.secret = secret;
  }

  public String getSecret() {
    return secret;
  }

  @Override
  public String getMessage() {
    return String.format("Secret %s was not found", this.secret);
  }
}
