package de.max.micro.deploy.domain.service;

import javax.ws.rs.NotFoundException;

public class ServiceNotFoundException extends NotFoundException {

  final String service;

  public ServiceNotFoundException(final String service) {
    super();
    this.service = service;
  }

  public ServiceNotFoundException(final Throwable cause, final String service) {
    super(cause);
    this.service = service;
  }

  @Override
  public String getMessage() {
    return String.format("Service %s was not found", this.service);
  }

  public String getService() {
    return this.service;
  }
}
