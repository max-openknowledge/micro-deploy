package de.max.micro.deploy.domain.secret;

import de.max.micro.deploy.application.secret.SecretDTO;
import de.max.micro.deploy.domain.namespace.NamespaceHelper;
import de.max.micro.deploy.infrastructure.stereotypes.Service;
import io.fabric8.kubernetes.api.model.Namespace;
import io.fabric8.kubernetes.api.model.Secret;
import io.fabric8.kubernetes.api.model.SecretBuilder;
import io.fabric8.kubernetes.api.model.SecretList;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;

import java.util.List;
import java.util.Optional;

@Service
public class SecretService {
  private KubernetesClient client;

  public SecretService() {
    this.client = new DefaultKubernetesClient();
  }

  public SecretService(final KubernetesClient client) {
    this.client = client;
  }

  public List<Secret> listSecrets(final Namespace namespace) {
    return client
        .secrets()
        .inNamespace(namespace
            .getMetadata()
            .getName())
        .list()
        .getItems();
  }

  public Secret getSecretByName(final String secret, final Namespace namespace) {
    SecretList secrets = client
        .secrets()
        .inNamespace(namespace
            .getMetadata()
            .getName())
        .list();

    Optional<Secret> s = secrets
        .getItems()
        .stream()
        .filter(sec -> sec
            .getMetadata()
            .getName()
            .equals(secret))
        .findFirst();

    if (!s.isPresent()) {
      throw new SecretNotFoundException(secret);
    }

    return s.get();
  }

  public Secret createSecret(final SecretDTO secretDTO) {
    return new SecretBuilder()
        .withNewMetadata()
        .withName(secretDTO.getName())
        .addToLabels(secretDTO.getLabels())
        .endMetadata()
        .withData(secretDTO.getData())
        .build();
  }

  public Secret applySecret(final Secret secret, final Namespace namespace) {
    return client
        .secrets()
        .inNamespace(namespace
            .getMetadata()
            .getName())
        .create(secret);
  }

  public boolean deleteSecret(final Secret secret, final String namespace) {
    return client
        .secrets()
        .inNamespace(namespace)
        .delete(secret);
  }

  public Namespace getNamespace(final String namespace) {
    return NamespaceHelper.getNamespace(client, namespace);
  }
}
